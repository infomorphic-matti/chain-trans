# chain-trans

Simple rust utility library for applying functions to structures in chains, and chaining functions together (also, trans rights! 🏳️‍⚧️)).

This lets you write code in fluent style with essentially arbitrary functions - with benefits to readability (in particular, avoiding messy intermediary variables when you use free functions), ease of logging, and structuring code in terms of data transformations.

This crate is inspired by the pleasantness of chaining calls within the framework of rust iterators, maps, and filters. 

# Examples

```rust
use chain_trans::prelude::*;

pub struct Point(f32, f32);

let parameterised_point = 3.0f32
    .trans(|a| Point(a, a * 2))
    .trans_inspect(|Point(x, y)| eprintln!("Current point is at {x}, {y}"));
```


