//! Library for conveniently combining and applying to structures.
//!
//! See [`Trans`] for more info.
#![no_std]

/// Basic, universal transformations on types.
///
/// The `trans*` functions take *owned* structures and return other *owned* structures - i.e. they
/// are a *transformation* of a structure that acts like a pipeline. Because this consumes the
/// structures these functions are only implemented on [`Sized`] types.
///
/// The `chain*` functions allow you to chain functions that take their argument by reference
/// (shared or mutually-exclusive), without needing temporary variables. These functions always
/// return a reference to [`Self`] of some kind.  
pub trait Trans {
    /// Apply a transformation function from self to any arbitrary type, consuming `self`.
    ///
    /// Allows chaining function calls on an object in-place, similar to how you might use
    /// [`core::iter::Iterator::map`] but on arbitrary expressions and types.
    ///
    /// # Examples
    ///
    /// Basic example of operational flow.
    ///
    /// ```rust
    /// use chain_trans::Trans;
    ///
    /// let bare_integer = 32;
    /// let as_some_and_halved = bare_integer
    ///     .trans(|a| a >> 1)
    ///     .trans(Some);
    ///
    /// assert_eq!(as_some_and_halved, Some(16));
    /// ```
    #[inline]
    fn trans<T>(self, to_apply: impl FnOnce(Self) -> T) -> T
    where
        Self: Sized,
    {
        to_apply(self)
    }

    /// Apply a function that works on mutually exclusive references to self as if it were an
    /// ownership-taking transformation like you would pass to [`Trans::trans`]
    #[inline]
    fn trans_mut(mut self, to_apply: impl FnOnce(&mut Self)) -> Self
    where
        Self: Sized,
    {
        to_apply(&mut self);
        self
    }

    /// Apply a function that works on shared references to self as a consuming transformation, in
    /// the style of [`Trans::trans`]
    ///
    /// # Examples
    /// Easy inline logging.
    ///
    /// ```rust
    /// use chain_trans::Trans;
    ///
    /// let final_val = 3u64
    ///     .trans(|a| a * 3)
    ///     .trans(|a| a + 4)
    ///     .trans_inspect(|intermediary_val| eprintln!("Intermediary value is {intermediary_val:#?}"))
    ///     .trans(|a| a * 2)
    ///     .trans(Some)
    ///     .trans_inspect(|final_val| eprintln!("We eventually got {final_val:#?}"));
    /// ```
    #[inline]
    fn trans_inspect(self, to_apply: impl FnOnce(&Self)) -> Self
    where
        Self: Sized,
    {
        to_apply(&self);
        self
    }

    /// Modify the object reference with the given function, returning the object reference
    /// once again. This turns arbitrary functions into fluent-style calls.
    ///
    /// # Examples
    /// Adding environment variables to a [`std::process::Command`] in-place before spawning.
    /// ```rust,no_run
    /// # fn main() -> Result<(), std::io::Error> {
    /// use chain_trans::Trans;
    /// use std::process::Command;
    ///
    /// pub fn set_my_env_var(cmd: &mut Command) {
    ///     cmd.env("MY_SPECIAL_VAR", "MY_SPECIAL_VAL");
    /// }
    ///
    /// let mut child_proc = Command::new("ls")
    ///     .arg("-l")
    ///     .chain_mut(set_my_env_var)
    ///     .spawn()?;
    /// child_proc.wait()?;
    /// # Ok(())
    /// # }
    /// ```
    #[inline]
    fn chain_mut(&mut self, to_apply: impl FnOnce(&mut Self)) -> &'_ mut Self {
        to_apply(self);
        self
    }

    #[inline]
    /// Inspect a shared reference to a structure with a function and
    /// return the reference again, for easy function chaining. It's uses are
    /// similar to that of [`Trans::trans_inspect`]
    fn chain_inspect(&self, to_apply: impl FnOnce(&Self)) -> &'_ Self {
        to_apply(self);
        self
    }
}

impl<T: ?Sized> Trans for T {}

/// Useful items to include in your dependant crate.
pub mod prelude {
    pub use super::Trans;
}
